Preliminaries
=============

.. toctree::
    :maxdepth: 2

    about
    whats_new
    authors
    license
    installation
    cite
