:orphan:

.. _alternative_boundary_conditions:

Alternative boundary conditions
===============================

.. jupyter-execute::
    :hide-code:

    # suppress jupyter warnings messages when calling kwant.plot()
    import matplotlib.pyplot, matplotlib.backends



The physical system in this example is similar to :ref:`open_system`.

**tkwant features highlighted**

- Selecting boundary conditions manually

- Impact of the boundary type on performance

.. jupyter-execute:: alternative_boundary_conditions.py

.. seealso::
    The complete source code of this example can be found in
    :download:`alternative_boundary_conditions.py <alternative_boundary_conditions.py>`.

